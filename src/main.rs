use std::borrow::Cow;
use std::io::prelude::*;
use std::fs::File;
use std::path::PathBuf;
use structopt::StructOpt;
use strong_xml::{XmlRead, XmlWrite};
use serde::Serialize;

#[derive(StructOpt, Debug)]
enum Type {
    Name,
    Path,
    Repo,
}

impl std::str::FromStr for Type {
    type Err = std::num::ParseIntError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use crate::Type::*;
        match s {
            "name" => Ok(Name),
            "repo" => Ok(Repo),
            "path" => Ok(Path),
            _ => panic!("{} not supported", s),
        }
    }
}

#[derive(StructOpt, Debug)]
#[structopt(name = "command")]
enum Command {
    Print{
        #[structopt(default_value="name")]
        kind: Type,
    },
}

#[derive(StructOpt)]
struct Args {
    #[structopt(long)]
    manifest: PathBuf,
    #[structopt(subcommand)]
    command: Command,
}

#[derive(Serialize, XmlWrite, XmlRead, PartialEq, Debug)]
#[xml(tag = "default")]
struct Def<'a> {
    #[xml(attr="remote")]
    remote: Cow<'a, str>,
    #[xml(attr="revision")]
    revision: Cow<'a, str>,
}

#[derive(Serialize, XmlWrite, XmlRead, PartialEq, Debug)]
#[xml(tag = "project")]
struct Project<'a> {
    #[xml(attr="name")]
    name: Cow<'a, str>,
    #[xml(attr="path")]
    path: Cow<'a, str>,
    #[xml(attr="revision")]
    revision: Cow<'a, str>,
    #[xml(attr="upstream")]
    upstream: Cow<'a, str>,
    #[xml(attr="remote")]
    remote: Option<Cow<'a, str>>,
}

#[derive(Serialize, XmlWrite, XmlRead, PartialEq, Debug)]
#[xml(tag = "remote")]
struct Remote<'a>  {
    #[xml(attr="fetch")]
    fetch: Cow<'a, str>,
    #[xml(attr="name")]
    name: Cow<'a, str>,
    #[xml(attr="review")]
    review: Cow<'a, str>,
}

#[derive(Serialize, XmlWrite, XmlRead, PartialEq, Debug)]
#[xml(tag = "manifest")]
struct Manifest<'a> {
    #[xml(child="project")]
    projects: Vec<Project<'a>>,
    #[xml(child="default")]
    def: Def<'a>,
    #[xml(child="remote")]
    remotes: Vec<Remote<'a>>,
}

impl <'a>Manifest<'a> {
    fn from_file(file: &PathBuf, trimmed: &'a mut String) -> Result<Self, std::io::Error> {
        let mut f = File::open(&file)?;
        let mut data = String::new();
        f.read_to_string(&mut data)?;
        for line in data.lines() {
            trimmed.push_str(&line.trim());
        }
        let mut m = Manifest::from_str(trimmed).expect("Deserialize error");
        for project in &mut m.projects {
            if project.remote.is_none() {
                project.remote = Some(m.def.remote.clone());
            }
        }

        Ok(m)
    }

    #[allow(dead_code)]
    fn repo(&self, project_name: &str) -> Option<String> {
        for project in &self.projects {
            if project.name == project_name {
                if let Some(remote) = &project.remote {
                    for r in &self.remotes {
                        if &r.name == remote {
                            return Some(format!("{}{}", r.fetch, project.name));
                        }
                    }
                }
            }
        }
        None
    }
}

fn main() {
    let args = Args::from_args();
    let mut trimmed = String::new();
    let m = Manifest::from_file(&args.manifest, &mut trimmed);
    if let Err(e) = m {
        eprintln!("'{}' {}",args.manifest.to_string_lossy(), e);
        std::process::exit(1);
    }
    let m = m.unwrap();
    match args.command {
        Command::Print{kind} => {
            for project in &m.projects {
                match &kind {
                    Type::Name => println!("{}", project.name),
                    Type::Path => println!("{}", project.path),
                    Type::Repo => println!("{}", m.repo(&project.name.clone().to_string()).unwrap()),
                }
            }
        }
    }
    /*
    for project in &mut m.projects {
        println!();

        if let Some(remote) = &project.remote {
            for r in &m.remotes {
                if &r.name == remote {
                    println!("git clone {}{} {}", r.fetch, project.name, project.path);
                    break;
                }
            }
        }

        println!("path: {} ", project.path);
        println!("revision: {}", project.revision);
        print!("remote: ");
        println!("{}", project.upstream);
    }
    */
}
